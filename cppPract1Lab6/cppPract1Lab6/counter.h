#ifndef COUNTER_H
#define COUNTER_H

class Counter
{
private:
	static int countOfObjects;
public:
	Counter();
	~Counter();
	int static getCount();
};
#endif