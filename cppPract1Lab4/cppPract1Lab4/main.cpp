#include <iostream>

int *adrK;

int* f1()
{
	int static k;
	k = k*k;
	return &k;
}

void f2()
{
	int n;
	std::cin>>n;
	*adrK = n;
}

int main()
{
	adrK = f1();
	std::cout<<adrK<<"  "<<*adrK<<std::endl;
	f2();
	std::cout<<adrK<<"  "<<*adrK<<std::endl;
	adrK = f1();
	std::cout<<adrK<<"  "<<*adrK<<std::endl;
	return 0;
}