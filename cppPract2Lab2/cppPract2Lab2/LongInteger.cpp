#include "LongInteger.h"
#include <cstring>

using namespace std;
LongInteger::~LongInteger()
{
	delete [] number;
}

LongInteger::LongInteger()
{
	lenght = 1;
	sign = '+';
	number = new int;
	(*number) = 0;
}

LongInteger::LongInteger(LongInteger& a)
{
	lenght = a.GetLenght();
	number = new int[lenght];
	for (int i = 0; i < lenght; i++)
	{
		number[i] = a[i];
	}
	sign = a.GetSign();
}

LongInteger::LongInteger(int len, int * source)
{
	lenght = len;
	number = new int[len];
	sign = '+';
	for (int i = 0; i < lenght; i ++)
	{
		number[i] = source[i];
	}
}

LongInteger::LongInteger(char * source)
{
	number = new int[strlen(source)];
	sign = '+';
	lenght = strlen(source);
	for (int i = 0; i < strlen(source); i ++)
	{
		number[i] = source[i] - '0';
	}
}


std::ostream& operator<<(std::ostream& output, LongInteger& lint)
{
	if (lint.GetSign() == '-')
	{
		output<<'-';
	}
	for (int i = 0; i < lint.GetLenght(); i ++)
	{
		output<<lint[i];
	}
	output<<std::endl;
	return output;
}

int * LongInteger::GetNumber()
{
	return number;
}

int LongInteger::GetLenght()
{
	return lenght;
}

void LongInteger::NormalazeP()
{
	bool flag = false;
	for (int i = lenght -1; i >=0; i --)
	{
		if (flag)
		{
			number[i] ++;
		}
		if (number[i] > 9)
		{
			flag = true;
			number[i] -= 10;
		}
		if ((i == 0)&&(flag))
		{
			lenght ++;
			int * tmp = new int[lenght];
			tmp[0] = 1;
			for (int j = 1; j < lenght; j++)
			{
				tmp[j] = number[j - 1];
			}
			delete [] number;
			number = tmp;
		}
	}
}

void LongInteger::NormalizeM()
{
	for (int i = lenght - 1; i > 0; i --)
	{
		if(number[i] < 0)
		{
			number[i] = 10 + number[i];
			number[i - 1] --;
		}
	}
}

void LongInteger::NormalizeN()
{
	if(number[0] == 0)
	{
		if (lenght == 1)
		{
			return;
		}
		else
		{
			lenght --;
			int * tmp = new int[lenght];
			for (int i = 0; i < lenght; i ++)
			{
				tmp[i] = number[i + 1];
			}
			delete[] number;
			number = tmp;
			NormalizeN();
		}
	}
	return;
}

LongInteger& LongInteger::operator+(LongInteger& a)
{
	char newsign;
	int newLen = (lenght>a.GetLenght())?lenght:a.GetLenght();
	int minLen = (lenght<a.GetLenght())?lenght:a.GetLenght();
	int * newNumb = new int[newLen];
	int * numbers = a.GetNumber();
	bool flag = true;
	if (sign == a.GetSign())
	{
		newsign = sign;
		int i = 0;
		for (i; i < minLen; i ++)
		{
			newNumb[newLen - 1 - i] = numbers[a.GetLenght() - 1 - i] + number[lenght - 1 - i];
		}
		if(minLen == lenght)
		{
			while(i < newLen)
			{
				newNumb[newLen - 1- i] = numbers[a.GetLenght() - 1 - i];
				i++;
			}
		}
		else
		{
			while(i < newLen)
			{
				newNumb[newLen - 1- i] = number[lenght - 1 - i];
				i++;
			}
		}
	}
	else
	{
		flag = false;
		LongInteger& tmp1 = this->abs();
		LongInteger& tmp2 = a.abs();
		if (tmp1 > tmp2)
		{
			newsign = sign;
			int i = 0;
			while(i < minLen)
			{
				newNumb[newLen - i - 1] = number[lenght - 1 - i] - numbers[minLen - 1 - i];
				i++;
			}
			while (i < lenght)
			{
				newNumb[lenght - 1 - i] = number[lenght - 1 - i];
				i++;
			}
		}
		else
		{
			newsign = a.GetSign();
			int i = 0;
			while(i < minLen)
			{
				newNumb[newLen - i - 1] =  - number[minLen - 1 - i] + numbers[newLen - 1 - i];
				i++;
			}
			while (i < newLen)
			{
				newNumb[newLen - 1 - i] = numbers[newLen - 1 - i];
				i++;
			}
		}
	}
	LongInteger * NewInt = new LongInteger(newLen, newNumb);
	if (flag)
	{
		NewInt->NormalazeP();
	}
	else
		NewInt->NormalizeM();
	if (newsign == '-')
	{
		NewInt->operator-();
	}
	return *NewInt;
}

LongInteger& LongInteger::abs()
{
	if (sign == '-')
	{
		LongInteger* tmpInt = new LongInteger(*this);
		tmpInt->operator-();
		return *tmpInt;
	}
	return *this;
}

LongInteger LongInteger::operator=(LongInteger& a)
{
	lenght = a.GetLenght();
	number = a.GetNumber();
	sign = a.GetSign();
	return *this;
}

int& LongInteger::operator[](int i)
{
	return number[i];
}

bool LongInteger::operator>(LongInteger& a)
{
	if ((*this) < a)
		return false;
	if ((*this) == a)
		return false;
	return true;
}

bool LongInteger::operator<(LongInteger& a)
{
	if ((sign == '-')&&(a.GetSign() == '+'))
	{
		return true;
	}
	bool flag;
	if (sign == a.GetSign())
	{
		if (sign == '+')
		{
			flag = true;
		}
		else
			flag = false;
		if (lenght > a.GetLenght())
			return !flag;
		if (lenght < a.GetLenght())
			return flag;
		int i = 0;
		while((i < lenght)&&(number[i] == a[i]))
		{
			i ++;
		}
		if ((i < lenght)&&(number[i] < a[i]))
			return flag;
	}
	return false;
}

bool LongInteger::operator==(LongInteger& a)
{
	if(sign != a.GetSign()) 
		return false;
	if (lenght != a.GetLenght())
		return false;
	int i = 0;
	while((i < lenght)&&(number[i] == a[i]))
	{
		i ++;
	}
	if (i == lenght)
		return true;
	return false;
}

bool LongInteger::operator!=(LongInteger& a)
{

	if (lenght != a.GetLenght())
		return true;
	int i = 0;
	while((i < lenght)&&(number[i] == a[i]))
	{
		i ++;
	}
	if (i == lenght)
		return false;
	return true;
}

LongInteger& LongInteger::operator-(LongInteger& a)
{
	LongInteger tmp = - a;
	LongInteger * newInt = new LongInteger((*this) + a);
	return *newInt;
}

LongInteger& LongInteger::operator++()
{
	number[lenght - 1] ++;
	NormalazeP();
	return *this;
}

LongInteger& LongInteger::operator-=(LongInteger& a)
{
	LongInteger tmpInt = - a;
	char newsign;
	int newLen = (lenght>a.GetLenght())?lenght:a.GetLenght();
	int minLen = (lenght<a.GetLenght())?lenght:a.GetLenght();
	int * newNumb = new int[newLen];
	int * numbers = a.GetNumber();
	bool flag = true;
	if (sign == tmpInt.GetSign())
	{
		newsign = sign;
		int i = 0;
		for (i; i < minLen; i ++)
		{
			newNumb[newLen - 1 - i] = numbers[tmpInt.GetLenght() - 1 - i] + number[lenght - 1 - i];
		}
		if(minLen == lenght)
		{
			while(i < newLen)
			{
				newNumb[newLen - 1- i] = numbers[tmpInt.GetLenght() - 1 - i];
				i++;
			}
		}
		else
		{
			while(i < newLen)
			{
				newNumb[newLen - 1- i] = number[lenght - 1 - i];
				i++;
			}
		}
	}
	else
	{
		flag = false;
		LongInteger& tmp1 = this->abs();
		LongInteger& tmp2 = tmpInt.abs();
		if (tmp1 > tmp2)
		{
			newsign = sign;
			int i = 0;
			while(i < minLen)
			{
				newNumb[newLen - i - 1] = number[lenght - 1 - i] - numbers[minLen - 1 - i];
				i++;
			}
			while (i < lenght)
			{
				newNumb[lenght - 1 - i] = number[lenght - 1 - i];
				i++;
			}
		}
		else
		{
			newsign = tmpInt.GetSign();
			int i = 0;
			while(i < minLen)
			{
				newNumb[newLen - i - 1] =  - number[minLen - 1 - i] + numbers[newLen - 1 - i];
				i++;
			}
			while (i < newLen)
			{
				newNumb[newLen - 1 - i] = numbers[newLen - 1 - i];
				i++;
			}
		}
	}
	delete [] number;
	number = newNumb;
	if (flag)
	{
		this->NormalazeP();
	}
	else
		this->NormalizeM();
	if (newsign == '-')
	{
		this->operator-();
	}
	return *this;
}

LongInteger& LongInteger::operator-()
{
	if (sign == '-')
	{
		sign = '+';
	}
	else 
		sign = '-';
	return *this;
}

char LongInteger::GetSign()
{
	return sign;
}