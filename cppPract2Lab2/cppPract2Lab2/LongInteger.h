#include <iostream>

class LongInteger
{
	int * number;
	char sign;
	int lenght;
public: 
	char GetSign();
	void NormalazeP();
	void NormalizeM();
	void NormalizeN();
	~LongInteger();
	LongInteger();
	LongInteger(LongInteger&);
	LongInteger(int, int*);
	LongInteger(char *);
	int * GetNumber();
	int  GetLenght();
	LongInteger& abs();
	friend std::ostream& operator<<(std::ostream& output, LongInteger& lint);
	int& operator[](int i);
	LongInteger& operator-(LongInteger& a);
	bool operator>(LongInteger& a);
	bool operator<(LongInteger& a);
	bool operator==(LongInteger& a);
	bool operator!=(LongInteger& a);
	LongInteger operator=(LongInteger& a);
	LongInteger& operator+(LongInteger& a);
	LongInteger& operator-();
	LongInteger& operator++();
	LongInteger& operator-=(LongInteger&);
};