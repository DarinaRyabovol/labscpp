#include <iostream>

bool isSimple(int n)
{
	int count = 0;
	for(int i = 1; i <= n; i ++)
	{
		if(n%i == 0)
			count ++;
	}
	if(count == 2)
		return true;
	return false;
}

int *myArray(int N, int& len)
{
	len = 0;
	int *tmpArray = new int[N - 1];
	for (int i = 0; i < N - 1; i++)
	{
		tmpArray[i] = 0;
	}
	for (int i = 2; i <= N; i++)
	{
		if(isSimple(i))
		{
			tmpArray[len] = i;
			len ++;
		}
	}
	int * arr = new int[len];
	for (int i = 0; i < len; i ++)
	{
		arr[i] = tmpArray[i];
	}
	delete(tmpArray);
	return arr;
}

int main()
{
	int N;
	int len;
	std::cin>>N;
	int *arr = myArray(N, len);
	std::cout<<len<<std::endl;
	for (int i = 0; i < len; i ++)
	{
		std::cout<<arr[i]<<"  ";
	}
	return 0;
}