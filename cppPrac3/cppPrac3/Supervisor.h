#include "Manager.h"
#include "MyList.h"
#include "MyString.h"

class Supervisor : public Employee
{
	MyList<Manager>* myManagers;
	float bonus;
public:
	MyList<Manager>* GetMyManagers();
	Supervisor(Supervisor&);
	Supervisor(MyString& Name, MyString& Surname, MyString& post, Date& d, float Bonus);
	Supervisor()
	{

	}
	void GiveBonus(Date& d);
	friend std::ostream& operator<<(std::ostream& output, Supervisor& sup);
	void Add(Manager& man);
	float GetBnus();
};