#include "Employee.h"
#include "Date.h"
#include "MyList.h"
#include <iostream>
#include "Supervisor.h"

int main()
{
	Date d1 = Date(2001, 10, 5);
	Employee emp(MyString("Ivan"), MyString("Ivanov"), MyString("post1"), d1, false);
	std::cout<<emp<<std::endl;
	Employee emp1(MyString("Sidor"), MyString("Sidorov"), MyString("post3"), d1, false);
	Manager manager(MyString("Petr"), MyString("Petrov"), MyString("post2"), Date(2001,1,12), 600);
	manager.AddNewEmpl(&emp,MyString("job1"), Date(), d1);
	manager.AddNewEmpl(&emp1,MyString("job2"),Date(2000, 12, 15), d1);
	std::cout<<manager<<std::endl;
	manager.GivePonus(d1);
	Supervisor supervisor(MyString("ololoshka"), MyString("olololoev"), MyString("post5"), Date(1999,10,10), false);
	supervisor.Add(manager);
	std::cout<<supervisor;
	return 0;
}