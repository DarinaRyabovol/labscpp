
#include "Employee.h"

using namespace std;

int Employee::counter;


std::ostream& operator<<(std::ostream& output, Employee& emp)
{
	output<<emp.GetName().GetChars()<<"  "<<emp.GetSurname().GetChars()<<"  "<<emp.GetPost().GetChars()<<"  number:"<<emp.GetCount();
	return output;
}

Employee::Employee(MyString& Name, MyString& Surname, MyString& Post, Date& d1, bool isCopy = true) : name(Name), surname(Surname), post(Post), hire(d1)
{
	number = counter;
	fire = Date();
	if(!isCopy)
		counter++;
}

MyString Employee::GetName()
{
	return  name;
}

MyString Employee::GetSurname()
{
	return surname;
}

MyString Employee::GetPost()
{
	return post;
}

Date Employee::GetHireDate()
{
	return hire;
}

Date Employee::GetFireDate()
{
	return fire;
}

void Employee::Fire(Date d)
{
	fire = d;
}

void Employee::ChangePost(MyString newPost)
{
	post = newPost;
}

Employee& Employee::operator=(Employee& emp)
{
	name = emp.GetName();
	surname = emp.GetSurname();
	post = emp.GetPost();
	number = emp.GetCount();
	hire = emp.GetHireDate();
	fire = emp.GetFireDate();
	return *this;
}

int Employee::GetCount()
{
	return number;
}

Employee::Employee(Employee& emp)
{
	name = emp.GetName();
	surname = emp.GetSurname();
	post = emp.GetPost();
	hire = emp.GetHireDate();
	fire = emp.GetFireDate();
	number = emp.GetCount();
}

