#include "Supervisor.h"


Supervisor::Supervisor(Supervisor& sup)
{
	Employee::Employee(sup);
	myManagers = sup.GetMyManagers();
	bonus = sup.GetBnus();
}
MyList<Manager>* Supervisor::GetMyManagers()
{
	return myManagers;
}
float Supervisor::GetBnus()
{
	return bonus;
}
Supervisor::Supervisor(MyString& Name, MyString& Surname, MyString& post, Date& d, float Bonus) : Employee(Name, Surname, post, d, false)
{
	bonus = Bonus;
	myManagers = NULL;
}
void Supervisor::GiveBonus(Date& d)
{
	MyList<Manager>* tmp = myManagers;
	while (tmp)
	{
		if (tmp->GetInfo().IsJobsReady(d))
		{
			std::cout<<tmp->GetInfo().GetCount()<<" has a bonus\n";
			tmp = tmp->GetNext();
		}
	}
}
std::ostream& operator<<(std::ostream& output, Supervisor& sup)
{
	output<<"Supervisor:\n\t"<<sup.GetName()<<"  "<<sup.GetSurname()<<"  "<<sup.GetPost()<<"  "<<sup.GetCount()<<std::endl;
	output<<"My managers:\n"<<sup.GetMyManagers();
	return output;
}
void Supervisor::Add(Manager& man)
{
	if (myManagers == NULL)
	{
		myManagers = new MyList<Manager>(man);
	}
	else
		myManagers->SetNext(new MyList<Manager>(man));
}
