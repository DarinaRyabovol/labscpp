#ifndef datedef
#define datedef
class Date
{
	int year;
	int month;
	int day;
public:
	Date(int a = 0, int b = 0, int c = 0) : year(a), month(b), day(c)
	{

	}
	Date(Date& d)
	{
		year = d.GetYear();
		month = d.GetMonth();
		day = d.GetDay();
	}
	int GetYear()
	{
		return year;
	}
	int GetMonth()
	{
		return month;
	}
	int GetDay()
	{
		return day;
	}
	Date& operator=(Date& d)
	{
		year = d.GetYear();
		month = d.GetMonth();
		day = d.GetDay();
		return *this;
	}
	bool operator<=(Date& d)
	{
		if(year <= d.GetYear())
			return true;
		if(year == d.GetYear())
		{
			if (month <= d.GetMonth())
			{
				return true;
			}
			if (month == d.GetMonth())
			{
				if (day <= d.GetDay())
				{
					return true;
				}
			}
		}
		return false;
	}
};
#endif