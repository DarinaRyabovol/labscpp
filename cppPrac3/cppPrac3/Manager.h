#include "Employee.h"
#include "MyList.h"

class EmployeeWithJobs: public Employee
{
	MyString job;
	Date start;
	Date finish;
	bool isJobReady;
public:
	EmployeeWithJobs()
	{

	}
	EmployeeWithJobs(Employee&, MyString Job, Date Start, Date Finsh);
	EmployeeWithJobs(EmployeeWithJobs& emp);
	EmployeeWithJobs& operator=(EmployeeWithJobs& source);
	friend std::ostream& operator<<(std::ostream& output, EmployeeWithJobs& empl);
	MyString& GetJob();
	Date& GetStartDate();
	Date& GetFinishDate();
	bool GetRedy();
	void SetRedy(bool status);
};

class Manager : public Employee
{
	MyList<EmployeeWithJobs> *myEmpls;
	float bonus;
public:
	float GetBonus();
	Manager()
	{
		myEmpls = NULL;
	}
	Manager(MyString Name, MyString Surname, MyString Post, Date d, float bonus);
	void AddNewEmpl(Employee * empl, MyString Job, Date Start, Date Finsh);
	MyList<EmployeeWithJobs>* GetMyEmpls();
	void GivePonus(Date&);
	Manager& operator=(Manager& man);
	bool IsJobsReady(Date& d);
	Manager(Manager& man);
	friend std::ostream& operator<<(std::ostream& output, Manager& man);
};