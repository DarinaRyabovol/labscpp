#ifndef employeedef
#define employeedef
#include "Date.h"
#include "MyString.h"
#include <iostream>

class Employee
{
	MyString name;
	MyString surname;
	MyString post;
	int number;
	Date hire;
	Date fire;
	static int counter;
public:
	Employee()
	{

	}
	Employee& operator=(Employee& emp);
	Employee(MyString& Name, MyString& Surname, MyString& post, Date& d1, bool isCopy);
	MyString GetName();
	MyString GetSurname();
	MyString GetPost();
	Date GetHireDate();
	Date GetFireDate();
	void Fire(Date d);
	void ChangePost(MyString newPost);
	friend std::ostream& operator<<(std::ostream& output, Employee& emp);
	Employee(Employee& emp);
	int GetCount();
};
#endif