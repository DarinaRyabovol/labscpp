#include "Manager.h"
#include "Employee.h"


Manager& Manager::operator=(Manager& man)
{
	Employee::operator=(man);
	bonus = man.GetBonus();
	myEmpls = man.GetMyEmpls();
	return *this;
}
float Manager::GetBonus()
{
	return bonus;
}
Manager::Manager(MyString Name, MyString Surname, MyString Post, Date d, float bonus) : Employee(Name, Surname, Post, d, false)
{
	myEmpls = NULL;
	this->bonus = bonus;
}

MyList<EmployeeWithJobs>* Manager::GetMyEmpls()
{
	return myEmpls;
}

void Manager::AddNewEmpl(Employee * empl, MyString Job, Date Start, Date Finsh)
{
	EmployeeWithJobs tmp(*empl, Job, Start, Finsh);
	if (myEmpls == NULL)
	{
		myEmpls = new MyList<EmployeeWithJobs>(tmp);
		return;
	}
	myEmpls->SetNext(new MyList<EmployeeWithJobs>(tmp));
}

EmployeeWithJobs::EmployeeWithJobs(EmployeeWithJobs& emp):Employee(emp)
{
	job = emp.GetJob();
	start = emp.GetStartDate();
	finish = emp.GetFinishDate();
	isJobReady = emp.GetRedy();
}

EmployeeWithJobs& EmployeeWithJobs::operator=(EmployeeWithJobs& source)
{
	Employee::operator=(source);
	job = source.GetJob();
	start = source.GetStartDate();
	finish = source.GetFinishDate();
	isJobReady = source.GetRedy();
	return *this;
}

EmployeeWithJobs::EmployeeWithJobs(Employee& empl, MyString Job, Date Start, Date Finsh):Employee(empl)
{
	job = Job;
	start = Start;
	finish = Finsh;
	isJobReady = false;
}

std::ostream& operator<<(std::ostream& output, Manager& man)
{
	output<<"Manager:  "<<man.GetName()<<"  "<<man.GetSurname()<<"  "<<man.GetPost()<<" "<<man.GetCount()<<std::endl;
	output<<"My employees:"<<std::endl;
	output<<man.GetMyEmpls();
	return output;
}

MyString& EmployeeWithJobs::GetJob()
{
	return job;
}

Date& EmployeeWithJobs::GetStartDate()
{
	return start;
}

Date& EmployeeWithJobs::GetFinishDate()
{
	return finish;
}

bool EmployeeWithJobs::GetRedy()
{
	return isJobReady;
}

void EmployeeWithJobs::SetRedy(bool status)
{
	isJobReady = status;
}

std::ostream& operator<<(std::ostream& output, EmployeeWithJobs& empl)
{
	output<<empl.GetName()<<"  "<<empl.GetSurname()<<" "<< empl.GetPost()<<"  "<<empl.GetJob()<<"  "<<empl.GetCount();
	return output;
}


bool Manager::IsJobsReady(Date& d)
{
	MyList<EmployeeWithJobs>* tmp = myEmpls;
	while (tmp)
	{
		if ((tmp->GetInfo().GetFinishDate() <= d)&&(tmp->GetInfo().GetRedy()))
		{
			tmp = tmp->GetNext();
		}
		else
			return false;
	}
	return true;
}

void Manager::GivePonus(Date& d)
{
	(myEmpls->GetInfo()).SetRedy(true);
	MyList<EmployeeWithJobs>* tmp = myEmpls;
	while(tmp)
	{
		if ((tmp->GetInfo().GetRedy())&&(tmp->GetInfo().GetFinishDate() <= d))
		{
			std::cout<<"employee number "<<tmp->GetInfo().GetCount()<<" has a bonus"<<std::endl;
		}
		tmp = tmp->GetNext();
	}
}

Manager::Manager(Manager& man) : Employee(man)
{
	myEmpls = man.GetMyEmpls();
	bonus = man.GetBonus();
}