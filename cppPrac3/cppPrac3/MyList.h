#ifndef mylistdef
#define  mylistdef
#include <iostream>
template <typename T> class MyList
{
	T info;
	MyList* next;
public:

	MyList(T& Info)
	{
		info = Info;
		next = NULL;
	}
	T GetInfo()
	{
		return info;
	}
	void SetNext(MyList* Next)
	{
		if (next == NULL)
		{
			next = Next;
			return;
		}
		next->SetNext(Next);
	}
	MyList* GetNext()
	{
		return next;
	}
	friend std::ostream& operator<<(std::ostream& output, MyList<T>* source)
	{
		MyList<T>* tmp = source;
		int i = 1;
		while(tmp)
		{
			output<<"\t"<<i<<"  "<<tmp->GetInfo()<<std::endl;
			i++;
			tmp = tmp->GetNext();
		}
		return output;
	}
};
#endif