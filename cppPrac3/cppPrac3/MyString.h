#ifndef mystringdef
#define  mystringdef
#include <iostream>

class MyString
{
	char* str;
	int lenght;
public:
	~MyString();
	MyString(int lenght);
	MyString();
	MyString(MyString& sourse);
	MyString(char *source);
	void Insert(int pose, MyString& source);
	MyString& GetSubStr(int start, int stop);
	int FindSubStr(MyString& str2);
	char& operator[](int i);
	char* GetChars();
	int GetLenght();
	MyString& operator+(MyString& str2);
	MyString& operator=(MyString& str2);
	friend std::ostream& operator<<(std::ostream & output, MyString& str);
};
#endif;